#include <string>
#include <ros/ros.h>
#include <evg55/gripper/EVG55.hpp>
#include <evg55/serial/RWHWSerialPort.hpp>
#include <kair_evg55/Home.h>
#include <kair_evg55/Open.h>


using namespace std;
using namespace evg55::gripper;
using namespace evg55::serial;


EVG55 g;


bool home(kair_evg55::Home::Request &req, kair_evg55::Home::Response &res)
{
  return g.home();
}


bool open(kair_evg55::Open::Request &req, kair_evg55::Open::Response &res)
{
  return g.move(req.pos);
}


int main(int argc, char *argv[])
{
  /* create node */
  ros::init(argc, argv, "kair_evg55");
  ros::NodeHandle nh("~");

  /* read parameters */
  string port;
  int baud_rate;
  int module_id;
  
  nh.param("port", port, string("/dev/ttyUSB0"));
  nh.param("baud_rate", baud_rate, 9600);
  nh.param("module_id", module_id, 12);
  
  /* create and connect to the gripper */
  RWHWSerialPort s;
  s.open(port, baud_rate);
  
  g.connect(&s, module_id);
  //g.home();
  
  /* launch service servers */
  ros::ServiceServer home_service = nh.advertiseService("home", home);
  ros::ServiceServer open_service = nh.advertiseService("open", open);
  
  /* launch a loop */
  ros::spin();

  return 0;
}

